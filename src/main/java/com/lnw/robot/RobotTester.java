/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.robot;

/**
 *
 * @author Maintory_
 */
public class RobotTester {
    public static void main(String[] args) {
        Robot robot1 = new Robot(0,0,99,1,100);
        System.out.println(robot1);
        robot1.walk1StepDirection('S');
        robot1.walk1StepDirection('N');
        System.out.println(robot1);
        robot1.walk1StepDirection('N');
        System.out.println(robot1);
        robot1.walk1StepDirection('E');
        System.out.println(robot1);
        robot1.walkToDirectionNStep('E' , 300);
        System.out.println(robot1);
        robot1.walkNStep(20);
        System.out.println(robot1);
        robot1.walkNStep(3);
    }
}
