/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.robot;

/**
 *
 * @author Maintory_
 */
public class Robot {
    private int x;
    private int y;
    private int bx;
    private int by;
    private int N;
    private char lastDirection = ' ';
    public Robot(int x,int y,int bx,int by,int N){
        this.x = x;
        this.y = y;
        this.bx = bx;
        this.by = by;
        this.N = N;
    }
    public boolean walk1StepDirection(char direction){
        switch (direction){
            case 'N':
                if(!inMap(x,y-1)){
                    cantMove();
                    return false;
                }
                y = y -1;
                break;
            case 'S':
                if(!inMap(x,y+1)){
                    cantMove();
                    return false;
                }
                y = y+1;
                break;
            case 'E':
                if(!inMap(x+1,y)){
                    cantMove();
                    return false;
                }
                x = x+1;
                break;
            case 'W':
                if(!inMap(x-1,y)){
                    cantMove();
                    return false;
                }
                x = x-1;
                break;
        }
        lastDirection = direction;
        isBomb();
        return true;
    }
    public boolean walkToDirectionNStep(char direction , int step){
        for(int i = 0 ; i<step; i++){
            if(!walk1StepDirection(direction)){
                return false;
            }
        }
        return true;
    }
    public boolean walkOneStep(){
        return walk1StepDirection(lastDirection);
    }
    public boolean walkNStep(int step){
        return walkToDirectionNStep(lastDirection , step);
    }
    public String toString(){
        return "Robot at (" + this.x + "," + this.y + ")";
    }
    public boolean inMap(int x , int y){
        if(x>=N || x<0 || y-1>=N|y-1<0){
            return false;
        }
            return true;
    }
    public void cantMove(){
        System.out.println("I cannot move it's over map !!!!!!!!!");
    }
    public void bombFound(){
        System.out.println("I found the bomb ! BOOM !!!");
    }
    public boolean isBomb(){
        if(x == bx && y == by){
        System.out.println("I found the bomb ! BOOM !!!");
            return true;
        }
            return false;
    }
}
